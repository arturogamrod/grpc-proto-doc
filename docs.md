# Protocol Documentation
<a name="top"></a>

## Table of Contents

- [gettrtap.proto](#gettrtap-proto)
    - [EmptyRequest](#gettrtap-EmptyRequest)
    - [GetPreferencesRequest](#gettrtap-GetPreferencesRequest)
    - [GetPreferencesResponse](#gettrtap-GetPreferencesResponse)
    - [HealthLivezResponse](#gettrtap-HealthLivezResponse)
    - [HealthReadyzResponse](#gettrtap-HealthReadyzResponse)
    - [LocationEnableDisableRequest](#gettrtap-LocationEnableDisableRequest)
    - [LocationEnableDisableResponse](#gettrtap-LocationEnableDisableResponse)
    - [SavePreferencesRequest](#gettrtap-SavePreferencesRequest)
    - [SavePreferencesResponse](#gettrtap-SavePreferencesResponse)
    - [TermsAcceptRejectRequest](#gettrtap-TermsAcceptRejectRequest)
    - [TermsAcceptRejectResponse](#gettrtap-TermsAcceptRejectResponse)
  
    - [HealthService](#gettrtap-HealthService)
    - [LocationService](#gettrtap-LocationService)
    - [PreferencesService](#gettrtap-PreferencesService)
    - [TermsService](#gettrtap-TermsService)
  
- [Scalar Value Types](#scalar-value-types)



<a name="gettrtap-proto"></a>
<p align="right"><a href="#top">Top</a></p>

## gettrtap.proto



<a name="gettrtap-EmptyRequest"></a>

### EmptyRequest







<a name="gettrtap-GetPreferencesRequest"></a>

### GetPreferencesRequest



| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| userId | [string](#string) |  |  |






<a name="gettrtap-GetPreferencesResponse"></a>

### GetPreferencesResponse



| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| _id | [string](#string) |  |  |
| userId | [string](#string) |  |  |
| search_unit | [string](#string) |  |  |
| search_radius | [float](#float) |  |  |
| _created_at | [string](#string) |  |  |
| _updated_at | [string](#string) |  |  |






<a name="gettrtap-HealthLivezResponse"></a>

### HealthLivezResponse



| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| live | [string](#string) |  |  |






<a name="gettrtap-HealthReadyzResponse"></a>

### HealthReadyzResponse



| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| ready | [string](#string) |  |  |






<a name="gettrtap-LocationEnableDisableRequest"></a>

### LocationEnableDisableRequest



| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| userId | [string](#string) |  |  |






<a name="gettrtap-LocationEnableDisableResponse"></a>

### LocationEnableDisableResponse



| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| success | [bool](#bool) |  |  |






<a name="gettrtap-SavePreferencesRequest"></a>

### SavePreferencesRequest



| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| userId | [string](#string) |  |  |
| search_unit | [string](#string) |  |  |
| search_radius | [float](#float) |  |  |






<a name="gettrtap-SavePreferencesResponse"></a>

### SavePreferencesResponse



| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| _id | [string](#string) |  |  |
| userId | [string](#string) |  |  |
| search_unit | [string](#string) |  |  |
| search_radius | [float](#float) |  |  |
| _created_at | [string](#string) |  |  |
| _updated_at | [string](#string) |  |  |






<a name="gettrtap-TermsAcceptRejectRequest"></a>

### TermsAcceptRejectRequest



| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| userId | [string](#string) |  |  |






<a name="gettrtap-TermsAcceptRejectResponse"></a>

### TermsAcceptRejectResponse



| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| success | [bool](#bool) |  |  |





 

 

 


<a name="gettrtap-HealthService"></a>

### HealthService


| Method Name | Request Type | Response Type | Description |
| ----------- | ------------ | ------------- | ------------|
| livez | [EmptyRequest](#gettrtap-EmptyRequest) | [HealthLivezResponse](#gettrtap-HealthLivezResponse) |  |
| readyz | [EmptyRequest](#gettrtap-EmptyRequest) | [HealthReadyzResponse](#gettrtap-HealthReadyzResponse) |  |


<a name="gettrtap-LocationService"></a>

### LocationService


| Method Name | Request Type | Response Type | Description |
| ----------- | ------------ | ------------- | ------------|
| enable | [LocationEnableDisableRequest](#gettrtap-LocationEnableDisableRequest) | [LocationEnableDisableResponse](#gettrtap-LocationEnableDisableResponse) |  |
| disable | [LocationEnableDisableRequest](#gettrtap-LocationEnableDisableRequest) | [LocationEnableDisableResponse](#gettrtap-LocationEnableDisableResponse) |  |


<a name="gettrtap-PreferencesService"></a>

### PreferencesService


| Method Name | Request Type | Response Type | Description |
| ----------- | ------------ | ------------- | ------------|
| save | [SavePreferencesRequest](#gettrtap-SavePreferencesRequest) | [SavePreferencesResponse](#gettrtap-SavePreferencesResponse) |  |
| get | [GetPreferencesRequest](#gettrtap-GetPreferencesRequest) | [GetPreferencesResponse](#gettrtap-GetPreferencesResponse) |  |


<a name="gettrtap-TermsService"></a>

### TermsService


| Method Name | Request Type | Response Type | Description |
| ----------- | ------------ | ------------- | ------------|
| accept | [TermsAcceptRejectRequest](#gettrtap-TermsAcceptRejectRequest) | [TermsAcceptRejectResponse](#gettrtap-TermsAcceptRejectResponse) |  |
| reject | [TermsAcceptRejectRequest](#gettrtap-TermsAcceptRejectRequest) | [TermsAcceptRejectResponse](#gettrtap-TermsAcceptRejectResponse) |  |

 



## Scalar Value Types

| .proto Type | Notes | C++ | Java | Python | Go | C# | PHP | Ruby |
| ----------- | ----- | --- | ---- | ------ | -- | -- | --- | ---- |
| <a name="double" /> double |  | double | double | float | float64 | double | float | Float |
| <a name="float" /> float |  | float | float | float | float32 | float | float | Float |
| <a name="int32" /> int32 | Uses variable-length encoding. Inefficient for encoding negative numbers – if your field is likely to have negative values, use sint32 instead. | int32 | int | int | int32 | int | integer | Bignum or Fixnum (as required) |
| <a name="int64" /> int64 | Uses variable-length encoding. Inefficient for encoding negative numbers – if your field is likely to have negative values, use sint64 instead. | int64 | long | int/long | int64 | long | integer/string | Bignum |
| <a name="uint32" /> uint32 | Uses variable-length encoding. | uint32 | int | int/long | uint32 | uint | integer | Bignum or Fixnum (as required) |
| <a name="uint64" /> uint64 | Uses variable-length encoding. | uint64 | long | int/long | uint64 | ulong | integer/string | Bignum or Fixnum (as required) |
| <a name="sint32" /> sint32 | Uses variable-length encoding. Signed int value. These more efficiently encode negative numbers than regular int32s. | int32 | int | int | int32 | int | integer | Bignum or Fixnum (as required) |
| <a name="sint64" /> sint64 | Uses variable-length encoding. Signed int value. These more efficiently encode negative numbers than regular int64s. | int64 | long | int/long | int64 | long | integer/string | Bignum |
| <a name="fixed32" /> fixed32 | Always four bytes. More efficient than uint32 if values are often greater than 2^28. | uint32 | int | int | uint32 | uint | integer | Bignum or Fixnum (as required) |
| <a name="fixed64" /> fixed64 | Always eight bytes. More efficient than uint64 if values are often greater than 2^56. | uint64 | long | int/long | uint64 | ulong | integer/string | Bignum |
| <a name="sfixed32" /> sfixed32 | Always four bytes. | int32 | int | int | int32 | int | integer | Bignum or Fixnum (as required) |
| <a name="sfixed64" /> sfixed64 | Always eight bytes. | int64 | long | int/long | int64 | long | integer/string | Bignum |
| <a name="bool" /> bool |  | bool | boolean | boolean | bool | bool | boolean | TrueClass/FalseClass |
| <a name="string" /> string | A string must always contain UTF-8 encoded or 7-bit ASCII text. | string | String | str/unicode | string | string | string | String (UTF-8) |
| <a name="bytes" /> bytes | May contain any arbitrary sequence of bytes. | string | ByteString | str | []byte | ByteString | string | String (ASCII-8BIT) |

