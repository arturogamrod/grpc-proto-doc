# Protocol Documentation

<a name="top"></a>

## Table of Contents

- [gettrtap.proto](#gettrtap-proto)
  - [HealthService](#gettrtap-HealthService)
  - [LocationService](#gettrtap-LocationService)
  - [PreferencesService](#gettrtap-PreferencesService)
  - [TermsService](#gettrtap-TermsService)
  - [EmptyRequest](#gettrtap-EmptyRequest)
  - [GetPreferencesRequest](#gettrtap-GetPreferencesRequest)
  - [GetPreferencesResponse](#gettrtap-GetPreferencesResponse)
  - [HealthLivezResponse](#gettrtap-HealthLivezResponse)
  - [HealthReadyzResponse](#gettrtap-HealthReadyzResponse)
  - [LocationEnableDisableRequest](#gettrtap-LocationEnableDisableRequest)
  - [LocationEnableDisableResponse](#gettrtap-LocationEnableDisableResponse)
  - [SavePreferencesRequest](#gettrtap-SavePreferencesRequest)
  - [SavePreferencesResponse](#gettrtap-SavePreferencesResponse)
  - [TermsAcceptRejectRequest](#gettrtap-TermsAcceptRejectRequest)
  - [TermsAcceptRejectResponse](#gettrtap-TermsAcceptRejectResponse)

<a name="gettrtap-proto"></a>

<p align="right"><a href="#top">Top</a></p>

## gettrtap.proto

<a name="gettrtap-HealthService"></a>

### HealthService

| Method Name | Request Type                           | Response Type                                          | Description     |
| ----------- | -------------------------------------- | ------------------------------------------------------ | --------------- |
| livez       | [EmptyRequest](#gettrtap-EmptyRequest) | [HealthLivezResponse](#gettrtap-HealthLivezResponse)   | liveness probe  |
| readyz      | [EmptyRequest](#gettrtap-EmptyRequest) | [HealthReadyzResponse](#gettrtap-HealthReadyzResponse) | readyness probe |

<a name="gettrtap-LocationService"></a>

### LocationService

| Method Name | Request Type                                                           | Response Type                                                            | Description                                                                        |
| ----------- | ---------------------------------------------------------------------- | ------------------------------------------------------------------------ | ---------------------------------------------------------------------------------- |
| enable      | [LocationEnableDisableRequest](#gettrtap-LocationEnableDisableRequest) | [LocationEnableDisableResponse](#gettrtap-LocationEnableDisableResponse) | Allows to track user's GPS location. Requires user to accepts terms and conditions |
| disable     | [LocationEnableDisableRequest](#gettrtap-LocationEnableDisableRequest) | [LocationEnableDisableResponse](#gettrtap-LocationEnableDisableResponse) | Disables user's location tracking                                                  |

<a name="gettrtap-PreferencesService"></a>

### PreferencesService

| Method Name | Request Type                                               | Response Type                                                | Description                                                                                                          |
| ----------- | ---------------------------------------------------------- | ------------------------------------------------------------ | -------------------------------------------------------------------------------------------------------------------- |
| save        | [SavePreferencesRequest](#gettrtap-SavePreferencesRequest) | [SavePreferencesResponse](#gettrtap-SavePreferencesResponse) | update user preferences for finding other users with same preferences. Requires user to accepts terms and conditions |
| get         | [GetPreferencesRequest](#gettrtap-GetPreferencesRequest)   | [GetPreferencesResponse](#gettrtap-GetPreferencesResponse)   | get user preferences. Requires user to accepts terms and conditions                                                  |

<a name="gettrtap-TermsService"></a>

### TermsService

| Method Name | Request Type                                                   | Response Type                                                    | Description                                                                                 |
| ----------- | -------------------------------------------------------------- | ---------------------------------------------------------------- |:------------------------------------------------------------------------------------------- |
| accept      | [TermsAcceptRejectRequest](#gettrtap-TermsAcceptRejectRequest) | [TermsAcceptRejectResponse](#gettrtap-TermsAcceptRejectResponse) | Allows application to store and use user's information. Location history, preferences, etc. |
| reject      | [TermsAcceptRejectRequest](#gettrtap-TermsAcceptRejectRequest) | [TermsAcceptRejectResponse](#gettrtap-TermsAcceptRejectResponse) | prevents application to store and use user's information.                                   |

<a name="gettrtap-EmptyRequest"></a>

### EmptyRequest

<a name="gettrtap-GetPreferencesRequest"></a>

### GetPreferencesRequest

| Field  | Type              | Label | Description                                         |
| ------ | ----------------- | ----- | --------------------------------------------------- |
| userId | [string](#string) |       | alphanumeric string: letters,numbers and underscore |

<a name="gettrtap-GetPreferencesResponse"></a>

### GetPreferencesResponse

| Field         | Type              | Label | Description       |
| ------------- | ----------------- | ----- | ----------------- |
| _id           | [string](#string) |       | database id       |
| userId        | [string](#string) |       | userId from gettr |
| search_unit   | [string](#string) |       |                   |
| search_radius | [float](#float)   |       |                   |
| _created_at   | [string](#string) |       |                   |
| _updated_at   | [string](#string) |       |                   |

<a name="gettrtap-HealthLivezResponse"></a>

### HealthLivezResponse

| Field | Type              | Label | Description |
| ----- | ----------------- | ----- | ----------- |
| live  | [string](#string) |       |             |

<a name="gettrtap-HealthReadyzResponse"></a>

### HealthReadyzResponse

| Field | Type              | Label | Description |
| ----- | ----------------- | ----- | ----------- |
| ready | [string](#string) |       |             |

<a name="gettrtap-LocationEnableDisableRequest"></a>

### LocationEnableDisableRequest

| Field  | Type              | Label | Description |
| ------ | ----------------- | ----- | ----------- |
| userId | [string](#string) |       |             |

<a name="gettrtap-LocationEnableDisableResponse"></a>

### LocationEnableDisableResponse

| Field   | Type          | Label | Description |
| ------- | ------------- | ----- | ----------- |
| success | [bool](#bool) |       |             |

<a name="gettrtap-SavePreferencesRequest"></a>

### SavePreferencesRequest

| Field         | Type              | Label | Description |
| ------------- | ----------------- | ----- | ----------- |
| userId        | [string](#string) |       |             |
| search_unit   | [string](#string) |       |             |
| search_radius | [float](#float)   |       |             |

<a name="gettrtap-SavePreferencesResponse"></a>

### SavePreferencesResponse

| Field         | Type              | Label | Description |
| ------------- | ----------------- | ----- | ----------- |
| _id           | [string](#string) |       |             |
| userId        | [string](#string) |       |             |
| search_unit   | [string](#string) |       |             |
| search_radius | [float](#float)   |       |             |
| _created_at   | [string](#string) |       |             |
| _updated_at   | [string](#string) |       |             |

<a name="gettrtap-TermsAcceptRejectRequest"></a>

### TermsAcceptRejectRequest

| Field  | Type              | Label | Description |
| ------ | ----------------- | ----- | ----------- |
| userId | [string](#string) |       |             |

<a name="gettrtap-TermsAcceptRejectResponse"></a>

### TermsAcceptRejectResponse

| Field   | Type          | Label | Description |
| ------- | ------------- | ----- | ----------- |
| success | [bool](#bool) |       |             |
